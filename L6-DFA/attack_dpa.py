import numpy as np
import matplotlib.pyplot as plt
import sys
from aes import *

#sys.path.append('/home/ubuntu/Desktop/physical_attacks_aes')
#import aes

def charger_tableau(nom_fichier):
    try:
        tableau = np.load(nom_fichier)
        return tableau
    except FileNotFoundError:
        print(f"Erreur : Le fichier {nom_fichier} n'a pas été trouvé.")
        return None

# Charger les tableaux
ciphertexts = charger_tableau("ciphertexts.npy")
faulty_ciphertexts = charger_tableau("faulty_ciphertexts.npy")
matrices_ciphertexts= np.reshape(ciphertexts, (ciphertexts.shape[0],4,4))
matrices_faulty_ciphertexts= np.reshape(faulty_ciphertexts, (faulty_ciphertexts.shape[0],4,4))

delta=[]
mix_columns=[]

print("taille: ",ciphertexts.shape)

for i in range(256):
    
    mix_columns=[[2,3,1,1],[1,2,3,1],[1,1,2,3],[3,1,1,2]]
    delta.append(np.dot(mix_columns,[i,0,0,0]))
    delta.append(np.dot(mix_columns,[0,i,0,0]))
    delta.append(np.dot(mix_columns,[0,0,i,0]))
    delta.append(np.dot(mix_columns,[0,0,0,i]))
delta=np.array(delta)
print("delta",delta)
cpt=0
# Vérifier si les tableaux ont été chargés avec succès
if ciphertexts is not None and faulty_ciphertexts is not None:
    # Afficher les dimensions des tableaux
    
    # Visualiser les 10 premières données (à titre d'exemple)
    exemples_a_afficher = ciphertexts.shape[0]
    
    for i in range(exemples_a_afficher):
        print(f"Exemple {i + 1}:")
        
        indices_diff=[]
        indices_diff = np.where(ciphertexts[i] != faulty_ciphertexts[i])
        if(len(indices_diff[0])!=0):
            cpt=cpt+1
            
            inv_ciphertexts=inv_shift_rows(matrices_ciphertexts[i])
            inv_faulty_ciphertexts=inv_shift_rows(matrices_faulty_ciphertexts[i])
            
            
            print()
            print("==SHIFT ROWS== ")
            print("Plaintext :", inv_ciphertexts)
            print("faulty_ciphertexts :", inv_faulty_ciphertexts)
            for j in range(256):
                k10 = [j,0,0,0]
                result1=[]
                xor_c=add_round_key(inv_ciphertexts[:,0],k10)
                xor_fc=add_round_key(inv_faulty_ciphertexts[:,0],k10)
                for c_sub,fc_sub in zip(inv_sub_bytes(xor_c), inv_sub_bytes(xor_fc)):
                    result1.append(c_sub^fc_sub)
                result1=np.array(result1)
                
                test=delta[:,0]
                print("test",test)
                print("result1",result1[0])
                condition = any(np.array_equal(result1[0], tableau) for tableau in delta[:,0])
                print(condition)
                if(condition):
                
                #if(result1[0] in delta[:,0]):
                    print("cond1")
                    for k in range(256):
                        k10 = [j,k,0,0]
                        result2=[]
                        xor_c=add_round_key(inv_ciphertexts[:,0],k10)
                        xor_fc=add_round_key(inv_faulty_ciphertexts[:,0],k10)
                        for c_sub,fc_sub in zip(inv_sub_bytes(xor_c), inv_sub_bytes(xor_fc)):
                           result2.append(c_sub^fc_sub)
                        result2=np.array(result2)
                        
                        condition = any(np.array_equal(result2[:2], tableau) for tableau in delta[:,:2])
                        
                        np.where(delta[:,:2]==result2[:2])
                        if(condition):
                        #if(result2[:2] in delta[:,:2]) :
                            print("cond2")
                            for l in range(256):
                                k10 = [j,k,l,0]
                                result3=[]
                                xor_c=add_round_key(inv_ciphertexts[:,0],k10)
                                xor_fc=add_round_key(inv_faulty_ciphertexts[:,0],k10)
                                for c_sub,fc_sub in zip(inv_sub_bytes(xor_c), inv_sub_bytes(xor_fc)):
                                    result3.append(c_sub^fc_sub)
                                result3=np.array(result3)
                               
                                condition = any(np.array_equal(result3[:3], tableau) for tableau in delta[:,:3])
                                if(condition):
                                # presence=np.isin(result3[:3],delta[:,:3])
                                # print(presence)
                                #if(result3[:3] in delta[:,:3]) :
                                    # print("result3",result3[:3])
                                    # print("delta3",delta[:,:3])
                                    for m in range(256):
                                        k10 = [j,k,l,m]
                                        result4=[]
                                        xor_c=add_round_key(inv_ciphertexts[:,0],k10)
                                        xor_fc=add_round_key(inv_faulty_ciphertexts[:,0],k10)
                                        for c_sub,fc_sub in zip(inv_sub_bytes(xor_c), inv_sub_bytes(xor_fc)):
                                            result4.append(c_sub^fc_sub)
                                        
                                        if(result4 in delta) :
                                            print("cond4")
                                               
# Obtenir les indices des lignes correspondantes dans autre_tableau

                                            
                #sigma=add_round_key(inv_sub_bytes(add_round_key(inv_ciphertexts[:,0],k10)),inv_sub_bytes(np.bitwise_xor(inv_faulty_ciphertexts[:,0],k10)))

        # for j in range(256):
        #    for k in range(256):
        #        for l in range(256):
        #            for m in range(256):
        #                k10=np.array([j,k,l,m])
        #   print(j)
    print("compteur", cpt)
        
else:
    print("Échec du chargement des tableaux.")
    


