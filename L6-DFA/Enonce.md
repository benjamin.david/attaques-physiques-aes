# DFA sur AES

Dans ce TP, vous devez réaliser des injections de fautes pour retrouver la clé de chiffrement. Pour cela, vous devrez réaliser une attaque selon votre préférence.

**Toutes les commandes bash sont à exécuter dans un terminal Nix**

## Identifier l’adresse de l’injection de faute

Le premier objectif est de trouver les paramètres de l'injection de faute : le premier est l’adresse de la faute. Le deuxième est l’itération : au bout de combien d’exécutions de l’instruction à l’adresse d’injection doit-on réellement réaliser la faute. Pour cela, vous disposez du script [K-fault-calibration.py](./K-fault-calibration.py) qui vous permet d’observer le résultat d’injections de faute.

Lancez le script avec

```bash
python3 ../L6-DFA/K-fault-calibration.py [cible] [itération] [symbole]
```
- La cible est le répertoire [aes_encrypt](../aes_encrypt/) contenant l’AES à évaluer.
- L’itération est le nombre au bout duquel, après tant d’exécutions de l’instruction à l’adresse choisie, l’injection a lieu.
- Le symbole est le nom de la fonction que vous voulez explorer.

**Q1)** Qu’affiche le script [K-fault-calibration.py](./K-fault-calibration.py) en sortie ?

**Q2)** Quelles itération et symbole faut-il choisir et pourquoi ?

**Q3)** Quelle adresse d’injection choisissez-vous ?

## Mesures

Le modèle de faute de notre simulation est un saut d’instruction avec une probabilité de succès dépendant de la somme des poids de Hamming des registres.

**Q4)** Quelles attaques théoriques compatibles avec ce modèle de faute connaissez-vous ? **Spécifiez l’attaque choisie pour la suite du TP.**

Pour réaliser une campagne de mesures, lancez le script
```bash
python3 ../L6-DFA/L-DFA.py [cible] [adresse] [nombre d’exécutions] [itération]
```

Les fichiers [ciphertexts.npy](../aes_encrypt/ciphertexts.npy) et [faulty_ciphertexts.npy](../aes_encrypt/faulty_ciphertexts.npy) sont alors générés.

**Q5)** Chargez les fichiers obtenus précédemment et comparez-les. Repérez quels octets ont été fautés. Sont-ils compatibles avec le modèle de faute théorique de l’attaque choisie ?

## Confrontation (dépend de l’attaque choisie)

**Q6)** Quelle est la cible de votre attaque, précisément ?

**Q7)** Décrivez le fonctionnement de votre attaque et son implémentation.

**Q8)** Concluez quelle est la bonne hypothèse de clé.

**Q9)** Si seule une partie de la clé a été obtenue, répétez l’opération pour retrouver la clé complète.

**Q10)** Comparez vos résultats avec la vraie clé.

**Q11)** Rédigez une conclusion en pointant les forces et faiblesses de cette attaque.