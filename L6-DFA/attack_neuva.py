import numpy as np
import matplotlib.pyplot as plt
import sys
from aes import *
from scipy.stats import entropy


# def shannon_entropy(column):
#     _, counts = np.unique(column, return_counts=True)
#     probas = counts / len(column)
#     return entropy(probas, base=2)

def calculer_entropie_colonnes(tableau):
    entropies = []

    for colonne in tableau.T:  # Transposer le tableau pour itérer à travers les colonnes
        proba_colonne = np.histogram(colonne, bins='auto', density=True)[0]  # Calculer la distribution de probabilités
        entropie_colonne = entropy(proba_colonne, base=2)  # Calculer l'entropie avec base 2
        entropies.append(entropie_colonne)

    return entropies

def shannon_entropy(column):
    unique_values, counts = np.unique(column, return_counts=True)
    probabilities = counts / len(column)
    entropy = -np.sum(probabilities * np.log2(probabilities))
    return entropy

def charger_tableau(nom_fichier):
    try:
        tableau = np.load(nom_fichier)
        return tableau
    except FileNotFoundError:
        print(f"Erreur : Le fichier {nom_fichier} n'a pas été trouvé.")
        return None

# Charger les tableaux
ciphertexts = charger_tableau("ciphertexts.npy")
faulty_ciphertexts = charger_tableau("faulty_ciphertexts.npy")
# matrices_ciphertexts= np.reshape(ciphertexts, (ciphertexts.shape[0],4,4))
# matrices_faulty_ciphertexts= np.reshape(faulty_ciphertexts, (faulty_ciphertexts.shape[0],4,4))
for n in range(5):

    print()
delta=[]
mix_columns=[]

# for i in range(256):
    
#     mix_columns=[[2,3,1,1],[1,2,3,1],[1,1,2,3],[3,1,1,2]]
#     delta.append(np.dot(mix_columns,[i,0,0,0]))
#     delta.append(np.dot(mix_columns,[0,i,0,0]))
#     delta.append(np.dot(mix_columns,[0,0,i,0]))
#     delta.append(np.dot(mix_columns,[0,0,0,i]))
# delta=np.array(delta)taille
taille=(ciphertexts.shape[0],256)
e=np.zeros(taille)

entropie_colonnes=np.zeros((16,256))
# Vérifier si les tableaux ont été chargés avec succès
if ciphertexts is not None and faulty_ciphertexts is not None:
    # Afficher les dimensions des tableaux
    for c in range (16):
        for n in range(ciphertexts.shape[0]):
            for possible_k in range(256):
                e[n][possible_k]=(InvSbox[ciphertexts[n][c]^possible_k])^(InvSbox[faulty_ciphertexts[n][c]^possible_k])
                
        #entropie_colonnes[c] = np.apply_along_axis(shannon_entropy, axis=0, arr=e)
        entropie_colonnes[c] = calculer_entropie_colonnes(e)
        # if(c==1):
        #     print(f"Entropie de la colonne: {np.argmin(entropies_colonnes)}")
        #     for i, entropie in enumerate(entropies_colonnes):
        #         if(entropie==np.argmin(entropies_colonnes)):
        #             print(f"Entropie de la colonne {i + 1}: {entropie}")
    minimums_lignes = np.argmin(entropie_colonnes, axis=1)

    # Afficher les résultats
    for i, minimum in enumerate(minimums_lignes):
        print(f"Minimum de la ligne {i + 1}: {minimum}")
    # compute the master key
    # matrice=[155,207,179,69,66,224,15,110,42,129,6,183,148,80,187,2]
    # print(k10tok0(matrice).hex())


else:
    print("Échec du chargement des tableaux.")
    


