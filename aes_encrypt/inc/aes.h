/**
* @Author: hélène, ronan
* @Date:   12-09-2016
* @Email:  helene.lebouder@inri.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 19-09-2016
* @License: GPL
*/



#ifndef AES_H
#define AES_H

#define DATA_SIZE       16
#define KEY_SIZE        16
#define STATE_ROW_SIZE  4
#define ROUND_COUNT     10

#include <stdint.h>

#define TRIGGER __asm("trigger:");

//encrypt plaintext with the key and put the result in ciphertext.
void aes_encrypt(uint8_t ciphertext[DATA_SIZE], uint8_t plaintext[DATA_SIZE], uint8_t key[KEY_SIZE]);


void AddRoundKey( uint8_t state1 [][STATE_ROW_SIZE], uint8_t state2 [][STATE_ROW_SIZE]);
void SubBytes(uint8_t state[][STATE_ROW_SIZE]);
void ShiftRows(uint8_t state[][STATE_ROW_SIZE]);
void MixColumns(uint8_t state [][STATE_ROW_SIZE]);

void KeyGen(uint8_t keys [][STATE_ROW_SIZE][STATE_ROW_SIZE], uint8_t master_key [][STATE_ROW_SIZE]);
void ColumnFill( uint8_t key[][STATE_ROW_SIZE][STATE_ROW_SIZE] , int round);
void OtherColumnsFill( uint8_t key [][STATE_ROW_SIZE][STATE_ROW_SIZE], int round);
void GetRoundKey( uint8_t round_key [][STATE_ROW_SIZE], uint8_t keys [][STATE_ROW_SIZE][STATE_ROW_SIZE], int round);

void MessageToState(uint8_t state [][STATE_ROW_SIZE], uint8_t message []);
void StateToMessage(uint8_t message [],uint8_t state [][STATE_ROW_SIZE]);
void MCMatrixColumnProduct(uint8_t column []);
uint8_t gmul(uint8_t a, uint8_t b);

extern const uint8_t rcon [10];

extern const uint8_t sboxtab [256];
extern const uint8_t invsbox [256];

extern uint8_t trigger_sel;

#endif
