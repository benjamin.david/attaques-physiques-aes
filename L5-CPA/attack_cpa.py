import numpy as np
import matplotlib.pyplot as plt
from aes import *
from gmpy2 import popcount
######################################### VISUALISATION DES TRACES ####################
def charger_tableau(nom_fichier):
    try:
        tableau = np.load(nom_fichier)
        return tableau
    except FileNotFoundError:
        print(f"Erreur : Le fichier {nom_fichier} n'a pas été trouvé.")
        return None

# Charger les tableaux
traces = charger_tableau("traces.npy")
plaintexts = charger_tableau("plaintexts.npy")
ciphertexts = charger_tableau("ciphertexts.npy")

# Vérifier si tous les tableaux ont été chargés avec succès
if traces is not None and plaintexts is not None and ciphertexts is not None:
    # Afficher les dimensions des tableaux
    print("Dimensions des traces :", traces.shape)
    print("Dimensions des plaintexts :", plaintexts.shape)
    print("Dimensions des ciphertexts :", ciphertexts.shape)

else:
    print("Échec du chargement des tableaux.")

######################################################### CPA ATTACK ################################
figure, axis = plt.subplots(4, 4) 
key=[]
for y in range(4):
    for z in range (4):
        prediction=[]
        for j in range(500):
            test2=[]
            for i in range(256):
                plain=text2matrix(plaintexts[j])
                test=sub_bytes(add_round_key(plain,i))
                test2.append(bin(test[y][z]).count('1'))
            prediction.append(test2)
                
        prediction=np.array(prediction)
        
        final_matrice=np.ones([255,510])
        for i in range(255):
            for j in range(510):
                corel=np.corrcoef(prediction[:,i],traces[:,j])
                final_matrice[i,j]=corel[0][1]
        final_matrice=np.array(final_matrice)
        #
        #figure, axis = plt.subplots(4, 4) 
        
        axis[y,z].plot(final_matrice)
        axis[y,z].set_title(f"octet {y*4+z+1}")

        indice_max = np.unravel_index(np.argmax(final_matrice, axis=None), final_matrice.shape)
        print("octet ", y*4+z+1 ," trouvé : ",hex(indice_max[0]),"à l'instruction :", indice_max[1])
        key.append(indice_max[0])
        

plt.show()
key_hex=[hex(num) for num in key]
print(key_hex)