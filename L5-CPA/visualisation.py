import numpy as np
import matplotlib.pyplot as plt

def charger_tableau(nom_fichier):
    try:
        tableau = np.load(nom_fichier)
        return tableau
    except FileNotFoundError:
        print(f"Erreur : Le fichier {nom_fichier} n'a pas été trouvé.")
        return None

# Charger les tableaux
traces = charger_tableau("traces.npy")
plaintexts = charger_tableau("plaintexts.npy")
ciphertexts = charger_tableau("ciphertexts.npy")

# Vérifier si tous les tableaux ont été chargés avec succès
if traces is not None and plaintexts is not None and ciphertexts is not None:
    # Afficher les dimensions des tableaux
    print("Dimensions des traces :", traces.shape)
    print("Dimensions des plaintexts :", plaintexts.shape)
    print("Dimensions des ciphertexts :", ciphertexts.shape)

    # Visualiser les 10 premières traces (à titre d'exemple)
    nombre_de_traces_a_visualiser = 1
    temps = np.arange(traces.shape[1])  # Temps pour l'axe des x

    for i in range(nombre_de_traces_a_visualiser):
        plt.plot(temps, traces[i], label=f"Trace {i + 1}")

    plt.title("Visualisation des Traces")
    plt.xlabel("Temps")
    plt.ylabel("Amplitude")
    plt.legend()
    plt.show()

    exemples_a_afficher = 5
    print(f"\nQuelques exemples de plaintexts et ciphertexts :\n")
    for i in range(exemples_a_afficher):
        print(f"Exemple {i + 1}:")
        print("Plaintext :", plaintexts[i])
        print("Ciphertext :", ciphertexts[i])
        print("trace :", traces[i])
        print("\n")


else:
    print("Échec du chargement des tableaux.")

